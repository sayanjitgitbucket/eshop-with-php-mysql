<?php
require 'includes/common.php';
$email = mysqli_real_escape_string($con,$_POST['email']);
$password = sha1(sha1(mysqli_real_escape_string($con,$_POST['password'])));
$query = "SELECT `id`,`name`,`email`,`password` FROM users";
$data = mysqli_query($con, $query) or die(mysqli_error($con));

while($data_fetched = mysqli_fetch_array($data)){
    if($data_fetched['email'] == $email && $data_fetched['password'] == $password){
        $_SESSION['id'] = sha1($data_fetched['id']);
        $_SESSION['email']=$data_fetched['email'];
        $_SESSION['name'] = $data_fetched['name'];
        header('location: products.php');
    }
}
if(!isset($_SESSION['id'])){
    header('location: login.php?info=incorrect email and password');
}