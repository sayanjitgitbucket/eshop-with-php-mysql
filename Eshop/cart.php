<!DOCTYPE html>
<?php require 'includes/common.php'; 
if(!isset($_SESSION['id'])){
    session_abort();
    header('location:index.php');
}else{
    $query = "SELECT `id`,`user_id`,`item_id`,`status` FROM users_item";
    $result = mysqli_query($con,$query);
?>
<html>
    <head>
        <title>Cart | LifestyleStore</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="bootstrap-3.3.7-dist/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
        <script src="bootstrap-3.3.7-dist/js/jquery-3.3.1.min.js" type="text/javascript"></script>
        <script src="bootstrap-3.3.7-dist/js/bootstrap.min.js" type="text/javascript"></script>
        <link href="bootstrap-3.3.7-dist/css/mycss.css" rel="stylesheet" type="text/css"/>
    </head>
    <body>
     <?php include 'includes/header.php'; ?>
        <br>
        <div class="container gap">
            <div class="row">
                <div class="col-sm-offset-3 col-sm-6">
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th>No.</th>
                                <th>Item name </th>
                                <th>Price</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $item_numb = 1; $price = 0;                             
                            while($fetched = mysqli_fetch_array($result)){
                                         if((sha1($fetched['user_id']) == $_SESSION['id'])&&($fetched['status'] == 'Added to cart')){
                                             $item_id = $fetched['item_id']; 
                                             $query2 = "SELECT `id`,`name`,`price` FROM item where id ='$item_id'";
                                             $result2 = mysqli_query($con, $query2);
                                             while($fetched2 = mysqli_fetch_array($result2)){ ?>
                            <tr>
                                <td><?php echo $item_numb;?></td>
                                <td><?php echo $fetched2['name'];?></td>
                                <td><?php echo $fetched2['price'];?>/-</td>
                                <td><a href="includes/remove_order.php?id=<?php echo $fetched2['id'];?>&cd=<?php echo $fetched['id'];?>" class="btn btn-sm btn-block btn-danger">Remove</a>
                            </tr>
<?php $item_numb++; $price += $fetched2['price']; } } } } ?>
                            <tr>
                                <td></td>
                                <td><b>TOTAL:</b></td>
                                <td><b><?php echo $price; ?>/-</b></td>
                            </tr>
                        </tbody>
                    </table><br>
                    <div class="col-sm-offset-4 col-sm-4"><a href="includes/confirm_order.php" class="btn btn-block btn-primary">Confirm Order</a></div>
                </div>
            </div>
        </div>
      <?php include 'includes/footer_fix.php'; ?>
    </body>
</html>
