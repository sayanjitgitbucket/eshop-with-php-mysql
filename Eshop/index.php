<?php require 'includes/common.php';
if (isset($_SESSION['email'])){
    header('location: products.php');
}
?>
<!DOCTYPE html>
<html>
    <head>
        <title>LifeStyle Store</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="bootstrap-3.3.7-dist/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
        <script src="bootstrap-3.3.7-dist/js/jquery-3.3.1.min.js" type="text/javascript"></script>
        <script src="bootstrap-3.3.7-dist/js/bootstrap.min.js" type="text/javascript"></script>
        <link href="bootstrap-3.3.7-dist/css/mycss.css" rel="stylesheet" type="text/css"/>
    </head>
    <body>
    <!-- navigation bar -->
    
    <?php include 'includes/header.php' ?>
    <div class='container-fluid'>
        <div class ='row'>
            <div class="col-sm-12 intro-img">
                    <div class="row">
                        <div class="col-sm-offset-3 col-sm-6">
                            <div class="jumbotron banner-content">
                                <h2>We sell lifestyle</h2><br>
                                <p>Flat 40% OFF on Premium Brands</p>
                                <a href="signup.php">
                                <input type="button" value="Shop now" class="btn btn-danger btn-lg active">
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>           
    <?php    include 'includes/footer.php'; ?>
    </body>
</html>
