-- phpMyAdmin SQL Dump
-- version 4.8.0.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Jun 01, 2018 at 07:51 AM
-- Server version: 10.1.31-MariaDB
-- PHP Version: 7.2.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `STORE`
--

-- --------------------------------------------------------

--
-- Table structure for table `item`
--

CREATE TABLE `item` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `price` int(11) NOT NULL,
  `category` enum('camera','watch','phone') DEFAULT NULL,
  `images` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `item`
--

INSERT INTO `item` (`id`, `name`, `price`, `category`, `images`) VALUES
(1, 'CANON', 36000, 'camera', 'img/camera/canon.jpg'),
(2, 'NIKON', 40000, 'camera', 'img/camera/nikon.jpg'),
(3, 'SONY', 45000, 'camera', 'img/camera/sony.jpg'),
(4, 'OLYMPUS', 50000, 'camera', 'img/camera/olympus.jpg'),
(5, 'ROLEX', 50000, 'watch', 'img/watches/rolex.jpg'),
(6, 'TAG HUER', 100000, 'watch', 'img/watches/tag_huer.jpg'),
(7, 'TOMMY HILFIGER', 20000, 'watch', 'img/watches/tommy.jpg'),
(8, 'TITAN', 10000, 'watch', 'img/watches/titan.jpg'),
(9, 'IPHONE X', 90000, 'phone', 'img/phones/iphone.jpg'),
(10, 'MI ONE PLUS', 35000, 'phone', 'img/phones/oneplus.jpg'),
(11, 'GOOGLE PIXEL', 65000, 'phone', 'img/phones/pixel.jpg'),
(12, 'SAMSUNG GALAXY S', 38000, 'phone', 'img/phones/samsung.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `test_users`
--

CREATE TABLE `test_users` (
  `id` int(12) NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `test_users`
--

INSERT INTO `test_users` (`id`, `name`, `email`, `password`) VALUES
(1, 'john smith', 'john@abc.com', 'john123'),
(2, 'ashley jones', 'ashley@abc.com', 'ashley123'),
(3, 'steven slate', 'steven@abc.com', 'steven123'),
(4, 'meera saigal', 'meera@abc.com', 'meera123'),
(5, 'bishnu ghosh', 'bishnu@qwe.com', '294dc371ef7a7d7fd1f291b1eb9cf422'),
(6, 'shalini sen', 'shalini@abc.com', '7bb6687be4c09693ced378f17133c850');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `contact` varchar(255) NOT NULL,
  `city` varchar(255) NOT NULL,
  `address` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `contact`, `city`, `address`) VALUES
(1, 'Sayanjit Das', 'sayanjit33@gmail.com', 'd86a80f100ab664efb3defd6cb83a5c3a2a9cea9', '9874577606', 'Kolkata', '1/59 Bijoygarh'),
(2, 'john smith', 'john@abc.com', '8753413eb957a0fc0846a3bee2fb77ba9d5af40d', '12345432', 'New York', 'West New York 55th street');

-- --------------------------------------------------------

--
-- Table structure for table `users_item`
--

CREATE TABLE `users_item` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `item_id` int(11) NOT NULL,
  `status` enum('Added to cart','Order confirmed') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users_item`
--

INSERT INTO `users_item` (`id`, `user_id`, `item_id`, `status`) VALUES
(12, 1, 9, 'Order confirmed'),
(13, 1, 5, 'Order confirmed'),
(14, 2, 6, 'Added to cart'),
(15, 2, 11, 'Added to cart');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `item`
--
ALTER TABLE `item`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `test_users`
--
ALTER TABLE `test_users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users_item`
--
ALTER TABLE `users_item`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `item`
--
ALTER TABLE `item`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `test_users`
--
ALTER TABLE `test_users`
  MODIFY `id` int(12) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `users_item`
--
ALTER TABLE `users_item`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
