<!DOCTYPE html>
<?php
if(isset($_GET['info']))
{
$info = $_GET['info'];
$show = '<div class="alert alert-danger"><p>' . $info . '</p></div>';
}
else{
    $show = "<p><em>login to make a purchase</em></p>";
}
?>
<html>
    <head>
        <title>Login</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="bootstrap-3.3.7-dist/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
        <script src="bootstrap-3.3.7-dist/js/jquery-3.3.1.min.js" type="text/javascript"></script>
        <script src="bootstrap-3.3.7-dist/js/bootstrap.min.js" type="text/javascript"></script>
        <link href="bootstrap-3.3.7-dist/css/mycss.css" rel="stylesheet" type="text/css"/>
    </head>
    <body>
        <?php include 'includes/header.php'; ?>
        <div class="container">
            <div class="row">
                <div class="col-sm-offset-4 col-sm-4">
                        <div class="panel panel-primary panel-gap">
                        <div class="panel-heading">
                            <h4>LOGIN</h4>
                        </div>
                        <div class="panel-body">
                            <?php echo $show ?>
                            <form action="login_submit.php" method="post">
                                <div class="form-group">
                                <input type="email" class="form-control" name="email" placeholder="username" required> 
                                </div>
                                <div class="form-group">
                                <input type="password" class="form-control" name="password" placeholder="password" required>
                                </div>
                                <div class="form-group"> 
                                <input type="submit" class="form-control-static btn btn-primary" value="login">
                                </div>
                            </form>
                        </div>
                            <div class="panel-footer">
                                <p>Don't have account? <a href="signup.php">Register</a></p>
                            </div>
                    </div>
                </div>
            </div>
        </div>
        <?php include 'includes/footer_fix.php'; ?>
    </body>
</html>
