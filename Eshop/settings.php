<!DOCTYPE html>
<?php require 'includes/common.php'; 
if(!isset($_SESSION['id'])){
    session_abort();
    header('location:index.php');
}
?>
<html>
    <head>
        <title>Settings</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="bootstrap-3.3.7-dist/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
        <script src="bootstrap-3.3.7-dist/js/jquery-3.3.1.min.js" type="text/javascript"></script>
        <script src="bootstrap-3.3.7-dist/js/bootstrap.min.js" type="text/javascript"></script>
        <link href="bootstrap-3.3.7-dist/css/mycss.css" rel="stylesheet" type="text/css"/>
    </head>
    <body>
        <?php include 'includes/header.php'; ?>
        <div class="container gap">
            <div class="row">
                <div class="col-sm-offset-4 col-sm-4">
                    <h3>Change password</h3>
                    <?php
                    if(isset($_GET['status']))
                    {
                    $info = $_GET['status'];
                    echo '<div class="alert alert-danger"><p>' . $info . '</p></div>';
                    } ?>
                    <form action="includes/settings_submit.php" method="POST">
                        <div class="form-group">
                            <input type="password" class="form-control" name="old_pass" placeholder="old password">
                        </div>
                        <div class="form-group">
                            <input type="password" class="form-control" name="new_pass" placeholder="new password">
                        </div>
                        <div class="form-group">
                            <input type="password" class="form-control" name="conf_pass" placeholder="confirm password">
                        </div>
                        <div class="form-group">
                        <input type="submit" class="form-control-static btn btn-primary" value="Reset">
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <?php include 'includes/footer_fix.php' ?>;
    </body>
</html>
