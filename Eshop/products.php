<!DOCTYPE html>
<?php require 'includes/common.php'; 
if(!isset($_SESSION['id'])){
    session_abort();
    header('location:index.php');
}
include 'includes/adding_product.php';
?>
<html>
    <head>
        <title>Products</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="bootstrap-3.3.7-dist/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
        <script src="bootstrap-3.3.7-dist/js/jquery-3.3.1.min.js" type="text/javascript"></script>
        <script src="bootstrap-3.3.7-dist/js/bootstrap.min.js" type="text/javascript"></script>
        <link href="bootstrap-3.3.7-dist/css/mycss.css" rel="stylesheet" type="text/css"/>
    </head>
    <body>
        <?php include 'includes/header.php'; ?>
        <div class="container">
            <div class="jumbotron gap-product ">
                <h1>Welcome to our LifeStyle Store!</h1>
                <p>We have the best cameras,watches and phones.No need to hunt around.We have all in one place.</p>
            </div>
        </div>
        <div class="container">
            <div class="row">
                <?php while($fetched = mysqli_fetch_array($result)){ ?>
                <div class="col-sm-3">
                    <div class="thumbnail thumb-back thumbnail_hover">
                        <img src=<?php echo $fetched['images']; ?> alt=<?php echo $fetched['name'];?>/>
                            <div class="caption"><div class="h4"><?php echo $fetched['name'];?></div><p><?php echo $fetched['price'];?></p>
                            </div><a href="includes/check_if_added.php?item_id=<?php echo sha1($fetched['id']);?>" class="btn btn-block btn-primary">Add to Cart</a>                                         
                    </div>
                </div>
                <?php } ?>
            </div>
        </div>
        <?php include 'includes/footer.php'; ?>
    </body>
</html>
