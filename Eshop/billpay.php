<html>
    <head>
        <title>Bill pay</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="bootstrap-3.3.7-dist/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
        <script src="bootstrap-3.3.7-dist/js/jquery-3.3.1.min.js" type="text/javascript"></script>
        <script src="bootstrap-3.3.7-dist/js/bootstrap.min.js" type="text/javascript"></script>
        <link href="bootstrap-3.3.7-dist/css/mycss.css" rel="stylesheet" type="text/css"/>
    </head>
    <body>
        
        <div class="container">
            <div class="col-sm-offset-4 col-sm-offset-4">
                <h2>Thank you for Ordering...</h2>
                <p>Your order is being processed</p>
                <a href="products.php" class="btn btn-default">Continue Shopping</a>
                
            </div>
        </div>
    </body>
</html>

