<!DOCTYPE html>

<html>
    <head>
        <title>Registration</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="bootstrap-3.3.7-dist/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
        <script src="bootstrap-3.3.7-dist/js/jquery-3.3.1.min.js" type="text/javascript"></script>
        <script src="bootstrap-3.3.7-dist/js/bootstrap.min.js" type="text/javascript"></script>
        <link href="bootstrap-3.3.7-dist/css/mycss.css" rel="stylesheet" type="text/css"/>
    </head>
    <body>
     <?php include 'includes/header.php'; ?>
        <div class="container">
            <div class="row">
                <div class="col-sm-offset-4 col-sm-4 gap">
                    <h3>Sign Up</h3>
                    <form action="signup_submit.php" method="post">
                        <input type="text" class="form-control" name="full_name" placeholder="Full Name" required>
                        <label class="form-group"></label>
                        <input type="email" class="form-control" name="email" placeholder="Email" required>
                        <label class="form-group"></label>
                        <input type="password" class="form-control" name="password" placeholder="Password" required>
                        <label class="form-group"></label>
                        <input type="text" class="form-control" name="contact" placeholder="Contact">
                        <label class="form-group"></label>
                        <input type="text" class="form-control" name="city" placeholder="City">
                        <label class="form-group"></label>
                        <input type="text" class="form-control" name="address" placeholder="Address">
                        <br>
                        <input type="submit" class="btn btn-primary" value="Submit">
                        
                    </form>
                </div>
            </div>
        </div>
       <?php include 'includes/footer_fix.php'?>
    </body>
</html>
